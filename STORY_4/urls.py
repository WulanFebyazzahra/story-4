from django.conf.urls import url, include
from django.contrib import admin
from django.contrib.auth import views
from django.urls import path
from . import views
#url for app

urlpatterns = [
    path('', views.Home, name='home'),
    path('about/', views.about, name='about'),
    path('experiences/', views.experiences, name='experiences'),
    path('educational/', views.educational, name='educational'),
    path('skill/', views.skill, name='skill'),
    path('reach/', views.reach, name='reach'),
]
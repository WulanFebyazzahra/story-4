from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
# Enter your name here

# Create your views here.
def Home(request):
    context = {
        "navbar_black" : True
    }
    return render(request, 'Home.html', context)
    
def about(request):
    return render(request, 'About.html')
    
def experiences(request):
    return render(request, 'Experiences.html')

def educational(request):
    return render(request, 'Educational.html')

def skill(request):
    return render(request, 'Skills.html')

def reach(request):
    context = {
        "navbar_black" : True
    }
    return render(request, 'Reach.html', context)
